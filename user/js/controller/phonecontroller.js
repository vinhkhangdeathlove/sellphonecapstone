export let phoneController = {
  layThongTinTuForm: () => {
    let tenDienThoai = document.getElementById("tenDienThoai").value;
    let giaDienThoai = document.getElementById("giaDienThoai").value;
    let moTa = document.getElementById("moTa").value;
    let phone = {
      name: tenDienThoai,
      desc: moTa,
      price: giaDienThoai,
    };
    return phone;
  },

  showThongTinLenForm: (phone) => {
    document.getElementById("tenDienThoai").value = phone.name;
    document.getElementById("giaDienThoai").value = phone.price;
    document.getElementById("moTa").value = phone.desc;
  },
};
