const BASE_URL = "https://62c3da1eabea8c085a649421.mockapi.io/products";
export let phoneService = {
  layDanhSachPhone: () => {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },

  layThongTinChiTietPhone: (idPhone) => {
    return axios({
      url: `${BASE_URL}/${idPhone}`,
      method: "GET",
    });
  },
};

export let res = await axios({
  method: "GET",
  url: BASE_URL,
});
