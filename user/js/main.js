import { phoneService, res } from "./service/phoneService.js";

let phoneList = res.data;
let listCart = [];

document.myForm.addEventListener("submit", (e) => {
  e.preventDefault();
  var radios = myForm.type;
  console.log(radios.value);
  renderProduct(phoneList, radios.value);
});

let renderProduct = (list, type) => {
  let contentHTML = "";
  let content = "";
  list.forEach((item) => {
    if (type == item.type || type == "all") {
      console.log(true);
      content = `
      <div class="col-3 mb-5">
        <div class="card w-100">
          <img src=${item.img} class="card-img-top mt-4" alt="">
          <div class="card-body">
            <h5 class="card-title">${item.name}</h5>
            <div class="card-text">${
              item.desc.length < 50 ? item.desc : item.desc.slice(0, 50) + "..."
            }</div>
            <p class="card-price">${item.price}$</p>

            <button class="btn btn-primary" onClick="handleAddToCard(${
              item.id
            })">Add to cart</button>
          </div>
        </div>
      </div>
    `;
      contentHTML = contentHTML + content;
    } else {
      console.log(false);
    }
  });
  document.getElementById("products").innerHTML = contentHTML;
};

renderProduct(phoneList, "all");

let renderTable = (list) => {
  let contentHTML = `
                    <div class="modal-body" id="modal_body">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Image</th>
                            <th scope="col">Name</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Unit Price</th>
                            <th scope="col">Total amount</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                    `;
  let quantityCart = 0;
  let totalAmount = 0;
  list.forEach((item) => {
    quantityCart += item.soLuong;
    totalAmount += item.soLuong * item.price;
    let contentTr = `<tr> 
                         <td> ${item.id} </td>
                         <td style="width: 70px"> <img src=${
                           item.img
                         } class="w-100" alt="" /></td>
                         <td> ${item.name} </td>
                         <td> 
                            <button onclick="handleChangeQuantity(${
                              item.id
                            }, -1)" class="btn btn-outline-success">-</button>
                            ${item.soLuong}
                            <button onclick="handleChangeQuantity(${
                              item.id
                            }, 1)" class="btn btn-outline-success">+</button>
                         </td>
                         <td> ${item.price}$ </td>
                         <td> ${item.soLuong * item.price}$ </td>
                         <td>    
                            <button onclick="handleRemovePhone(${
                              item.id
                            })" class="btn btn-warning">Xoá</button>
                         </td>
                     </tr>`;
    contentHTML = contentHTML + contentTr;
  });
  document.getElementById("cart_content").innerHTML =
    contentHTML +
    `
                        </tbody>
                      </table>
                    </div>
                    <div class="modal-footer bg-light">
                      <div class="lead mr-auto">
                        <h4>
                          Tổng số tiền:
                          ${totalAmount}$
                        </h4>
                      </div>
                      <button
                        type="button"
                        class="btn btn-outline-secondary"
                        data-dismiss="modal"
                      >
                        Close
                      </button>
                      <button
                        onclick="thanhToan()"
                        type="button"
                        class="btn btn-primary"
                      >
                        Thanh Toán
                      </button>
                    </div>`;
  document.getElementById("quantity_cart").innerHTML = quantityCart;
};

let handleAddToCard = (idPhone) => {
  let index = listCart.findIndex((item) => {
    return item.id == idPhone;
  });
  if (index == -1) {
    let newPhone = { ...phoneList[idPhone - 1], soLuong: 1 };
    listCart.push(newPhone);
  } else {
    listCart[index].soLuong++;
  }
  renderTable(listCart);
  luuLocalStorage(listCart);
};

window.handleAddToCard = handleAddToCard;

let handleRemovePhone = (idPhone) => {
  let index = listCart.findIndex((item) => {
    return item.id == idPhone;
  });
  if (index != -1) {
    listCart.splice(index, 1);
  }
  renderTable(listCart);
  luuLocalStorage(listCart);
};

window.handleRemovePhone = handleRemovePhone;

let handleChangeQuantity = (idPhone, step) => {
  let index = listCart.findIndex((item) => {
    return item.id == idPhone;
  });

  listCart[index].soLuong += step;
  if (listCart[index].soLuong == 0) {
    listCart.splice(index, 1);
  }
  renderTable(listCart);
  luuLocalStorage(listCart);
};

window.handleChangeQuantity = handleChangeQuantity;

let thanhToan = () => {
  if (listCart.length > 0) {
    clearCart();
    document.getElementById("cart_content").innerHTML = "Thanh toán thành công";
  }
  luuLocalStorage(listCart);
};

window.thanhToan = thanhToan;

let clearCart = () => {
  listCart.splice(0);
  renderTable(listCart);
  luuLocalStorage(listCart);
};

window.clearCart = clearCart;

document.getElementById("cart").addEventListener("click", () => {
  renderTable(listCart);
});

let dataJson = localStorage.getItem("DanhSachGioHang");
if (dataJson !== null) {
  let arrayDSGH = JSON.parse(dataJson);

  arrayDSGH.forEach((item) => {
    listCart.push(item);
  });
  renderTable(listCart);
}

let luuLocalStorage = (listCart) => {
  let cartJSON = JSON.stringify(listCart);
  localStorage.setItem("DanhSachGioHang", cartJSON);
};
