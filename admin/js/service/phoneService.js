const BASE_URL = "https://62c3da1eabea8c085a649421.mockapi.io/products";
export let phoneService = {
  layDanhSachPhone: () => {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },

  xoaPhone: (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    });
  },

  themMoiPhone: (phone) => {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: phone,
    });
  },

  layThongTinChiTietPhone: (idPhone) => {
    return axios({
      url: `${BASE_URL}/${idPhone}`,
      method: "GET",
    });
  },

  capNhatPhone: (phone) => {
    return axios({
      url: `${BASE_URL}/${phone.id}`,
      method: "PUT",
      data: phone,
    });
  },
};
