import { phoneService } from "./service/phoneService.js";

import { spinnerService } from "./service/spinnerService.js";
import { phoneController } from "./controller/phonecontroller.js";

let phoneList = [];

let idPhoneEdited = null;

let xoaPhone = (maPhone) => {
  spinnerService.batLoading();
  phoneService
    .xoaPhone(maPhone)
    .then((res) => {
      spinnerService.tatLoading();

      renderDanhSachService();
    })
    .catch((err) => {
      spinnerService.tatLoading();
    });
};
window.xoaPhone = xoaPhone;

let layChiTietPhone = (idPhone) => {
  idPhoneEdited = idPhone;
  spinnerService.batLoading();
  phoneService
    .layThongTinChiTietPhone(idPhone)
    .then((res) => {
      spinnerService.tatLoading();
      phoneController.showThongTinLenForm(res.data);
    })
    .catch((err) => {
      spinnerService.tatLoading();
    });
};

window.layChiTietPhone = layChiTietPhone;

let renderTable = (list) => {
  let contentHTML = "";
  for (let index = 0; index < list.length; index++) {
    let phone = list[index];
    let contentTr = `<tr> 
                         <td> ${phone.id} </td>
                         <td style="width: 70px"> <img class="w-100" src="${phone.img}" alt=""/> </td>
                         <td> ${phone.name} </td>
                         <td> ${phone.price} </td>
                         <td> ${phone.desc} </td>
                         <td> ${phone.type} </td>
                         <td>    
                                  <button
                                  onclick="layChiTietPhone(${phone.id})"
                                  class="btn btn-primary">Sửa</button>
                                  <button
                                  onclick="xoaPhone(${phone.id})"
                                  class="btn btn-warning">Xoá</button>
                         </td>
                     </tr>`;
    contentHTML = contentHTML + contentTr;
  }
  document.getElementById("tbody_phone").innerHTML = contentHTML;
};
let renderDanhSachService = () => {
  spinnerService.batLoading();
  phoneService
    .layDanhSachPhone()
    .then((res) => {
      spinnerService.tatLoading();

      phoneList = res.data;

      renderTable(phoneList);
    })
    .catch((err) => {
      spinnerService.tatLoading();
    });
};

renderDanhSachService();

let themPhone = () => {
  let phone = phoneController.layThongTinTuForm();

  spinnerService.batLoading();
  phoneService
    .themMoiPhone(phone)
    .then((res) => {
      spinnerService.tatLoading();
      renderDanhSachService();
    })
    .catch((err) => {
      spinnerService.tatLoading();

      alert("thất bại");
    });
};

window.themPhone = themPhone;

let capNhatPhone = () => {
  console.log("yes");
  let phone = phoneController.layThongTinTuForm();
  console.log("phone: ", phone);
  spinnerService.batLoading();

  let newPhone = { ...phone, id: idPhoneEdited };
  console.log("newPhone: ", newPhone);

  phoneService
    .capNhatPhone(newPhone)
    .then((res) => {
      spinnerService.tatLoading();
      phoneController.showThongTinLenForm({
        name: "",
        price: "",
        desc: "",
      });
      renderDanhSachService();
    })
    .catch((err) => {
      spinnerService.tatLoading();
      console.log(err);
    });
};

window.capNhatPhone = capNhatPhone;
