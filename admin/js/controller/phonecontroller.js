import {
  kiemTraRong,
  kiemTraChuVaSo,
  kiemTraSo,
  kiemTraGiaTri,
} from "../util/validation.js";

export let phoneController = {
  layThongTinTuForm: () => {
    let tenDienThoai = document.getElementById("tenDienThoai").value;
    let giaDienThoai = document.getElementById("giaDienThoai").value;
    let moTa = document.getElementById("moTa").value;
    let image = document.getElementById("image").value;
    let loaiDienThoai = document.getElementById("loaiDienThoai").value;

    var valid = true;
    //Kiểm tra dữ liệu rỗng

    valid &=
      kiemTraRong(
        tenDienThoai,
        "#error_required_tenDienThoai",
        "Tên điện thoại"
      ) &
      kiemTraRong(
        giaDienThoai,
        "#error_required_giaDienThoai",
        "Giá điện thoại"
      ) &
      kiemTraRong(image, "#error_required_linkDienThoai", "Link đường dẫn");

    //Kiểm tra chũ và số
    valid &= kiemTraChuVaSo(
      tenDienThoai,
      "#error_letter_number",
      "Tên điện thoại"
    );

    // Kiểm tra giá điện thoại
    valid &= kiemTraSo(giaDienThoai, "#error_all_number", "Giá điện thoại");

    //Kiểm tra mức giá tới thiểu
    valid &= kiemTraGiaTri(
      giaDienThoai,
      "#error_min_number",
      "Giá điện thoại",
      0
    );

    if (!valid) {
      return;
    } else {
      let phone = {
        name: tenDienThoai,
        desc: moTa,
        price: giaDienThoai,
        img: image,
        type: loaiDienThoai,
      };
      return phone;
    }
  },

  showThongTinLenForm: (phone) => {
    document.getElementById("tenDienThoai").value = phone.name;
    document.getElementById("giaDienThoai").value = phone.price;
    document.getElementById("moTa").value = phone.desc;
    document.getElementById("image").value = phone.img;
    document.getElementById("loaiDienThoai").value = phone.type;
  },
};
