export function kiemTraRong(value, selectorError, name) {
  if (value === "") {
    document.querySelector(selectorError).innerHTML =
      name + " không được bỏ trống";
    return false;
  }
  document.querySelector(selectorError).innerHTML = "";
  return true;
}

export function kiemTraChuVaSo(value, selectorError, name) {
  var regexLetter = /^[A-Z a-z 0-9]+$/;
  if (regexLetter.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).innerHTML =
    name + " phải chỉ gồm chữ cái hoặc số";
  return false;
}

export function kiemTraSo(value, selectorError, name) {
  var regexNumber = /^[0-9]+$/;
  if (regexNumber.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).innerHTML = name + " chỉ bao gồm số";
  return false;
}

export function kiemTraGiaTri(value, selectorError, name, minValue) {
  if (Number(value) <= minValue) {
    document.querySelector(selectorError).innerHTML =
      name + " phải lớn hơn " + minValue;
    return false;
  }
  document.querySelector(selectorError).innerHTML = "";
  return true;
}
